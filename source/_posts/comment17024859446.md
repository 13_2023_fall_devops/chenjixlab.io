# PR-31-2023-12-14
PR address: https://git.nju.edu.cn/13_2023_fall_devops/13_2023_fall_devops_server/-/merge_requests/31

Generated at: 2023-12-14 00:45
## src/test/java/org/fffd/l23o6/service/impl/RouteServiceImplTest.java
### Code
```java
// File Position
src/test/java/org/fffd/l23o6/service/impl/RouteServiceImplTest.java
// Method for Code Summary Begins
@Test
    void testListRoutes() {
        // Setup
        final RouteVO routeVO = new RouteVO();
        routeVO.setId(0L);
        routeVO.setName("name");
        routeVO.setStationIds(List.of(0));
        final List<RouteVO> expectedResult = List.of(routeVO);

        // Configure RouteDao.findAll(...).
        final List<RouteEntity> routeEntities = List.of(RouteEntity.builder()
                .name("name")
                .id(0L)
                .stationIds(List.of(0L))
                .build());
        when(mockRouteDao.findAll(Sort.by("name"))).thenReturn(routeEntities);

        // Run the test
        final List<RouteVO> result = routeServiceImplUnderTest.listRoutes();
        assertThat(result).isEqualTo(expectedResult);
        assertThat(result).isEqualTo(expectedResult);

        // Verify the results
    }
// Method for Code Summary Ends
```
### Summary
摘要：这是一个测试方法，用于测试RouteServiceImpl的listRoutes方法是否正确返回RouteVO对象。

输入：输入参数为空，但是该方法内部使用了mockRouteDao的findAll方法，其输入参数为Sort.by("name")。

返回值：返回值是RouteVO对象的列表，每个RouteVO对象的id、name和stationIds属性应该与输入的RouteEntity对象相同。
## src/test/java/org/fffd/l23o6/service/impl/RouteServiceImplTest.java
### Code
```java
// File Position
src/test/java/org/fffd/l23o6/service/impl/RouteServiceImplTest.java
// Method for Code Summary Begins
@Test
    void testDeleteRoute() {
        // Setup
        // Run the test
        routeServiceImplUnderTest.deleteRoute(0L);
        verify(mockRouteDao).deleteById(0L);
        verify(mockRouteDao).deleteById(0L);

        // Verify the results
    }
// Method for Code Summary Ends
```
### Summary
摘要：此方法是测试`routeServiceImplUnderTest`的`deleteRoute`方法是否正确删除了指定id的路线。

输入：`0L`，表示要删除的路线的id。

返回值：无返回值。此方法是用于测试的，因此没有返回值描述。
