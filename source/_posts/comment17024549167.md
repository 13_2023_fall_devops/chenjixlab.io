# PR-28-Update HelloController.java
PR address: https://git.nju.edu.cn/13_2023_fall_devops/13_2023_fall_devops_server/-/merge_requests/28

Generated at: 2023-12-13 16:08
## src/main/java/org/fffd/l23o6/controller/HelloController.java
### Code
```java
// File Position
src/main/java/org/fffd/l23o6/controller/HelloController.java
// Method for Code Summary Begins
@GetMapping("/hello")
    public String hello() {

        String str = "Hello, world!!!";


        return str;
    }
// Method for Code Summary Ends
```
### Summary
摘要：这个函数获取一个HTTP GET请求，并返回一个字符串。

输入：该函数的输入是一个HTTP GET请求。

返回值：该函数返回一个字符串，其内容是"Hello, world!!!"。
